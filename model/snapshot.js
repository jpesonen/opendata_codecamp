var mongoose = require('mongoose');

var snapshotSchema = new mongoose.Schema({
	noteId: String,
	title: String,
	note: String,
	created_at: Date


}); 



module.exports = mongoose.model('Snapshot', snapshotSchema);

