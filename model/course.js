var mongoose = require('mongoose');

var courseSchema = new mongoose.Schema({
    name: String,
    code: String,
    notes: [{ 
    	title: String,
    	note: String,
    	created_at: Date,
    	modified_at: Date,
    	
    }]
});

module.exports = mongoose.model('Course', courseSchema);