var app = angular.module('lectureNotes', ['btford.socket-io']).
factory('lecSocket', function (socketFactory) {
  return socketFactory();
});

var requestUrl = (location.hostname === 'localhost') ? '//localhost:8080' : '//'+location.hostname+':'+'8080';

function getCaretPosition (ctrl) {
	var CaretPos = 0;	// IE Support
	if (document.selection) {
	ctrl.focus ();
		var Sel = document.selection.createRange ();
		Sel.moveStart ('character', -ctrl.value.length);
		CaretPos = Sel.text.length;
	}
	// Firefox support
	else if (ctrl.selectionStart || ctrl.selectionStart == '0')
		CaretPos = ctrl.selectionStart;
	return (CaretPos);
};
function setCaretPosition(ctrl, pos){
	if(ctrl.setSelectionRange)
	{
		ctrl.focus();
		ctrl.setSelectionRange(pos,pos);
	}
	else if (ctrl.createTextRange) {
		var range = ctrl.createTextRange();
		range.collapse(true);
		range.moveEnd('character', pos);
		range.moveStart('character', pos);
		range.select();
	}
};

app.controller('courseController', function($scope, $http, lecSocket, $timeout){
	//change to retrieve from db
	$scope.courses = [];
	$scope.notes = [];
	$scope.snapshots = [];

	$scope.sentNotes = [];

	$scope.activeNote = null;
	$scope.activeCourseId = null;
	$scope.showRevision = false;
	var currentPos = 0;

	$http.get(requestUrl + '/courses')
	.success(function(response){ 
		$scope.courses = response;
		$scope.activeCourseId = response[0]._id;
		$scope.selectCourse(null, $scope.activeCourseId);
	});

	lecSocket.forward('edit-course', $scope);
	lecSocket.forward('edit', $scope);


	$scope.$on('socket:edit', function(event, data){
		var txt_area = document.querySelector('#note');
		if(getCaretPosition(txt_area) != txt_area.value.length)
		{
			currentPos = getCaretPosition(txt_area);
		}
		var oldLen = txt_area.value.length;

		var moveCaret = false;
		if($scope.sentNotes.indexOf(data.note) < 0){
			console.log('new note update');
			$scope.activeNote.title = data.title;
			$scope.activeNote.note = data.note;
			moveCaret = true;
		}
		
		if(document.activeElement == txt_area && moveCaret)
		{
			console.log("pos now: "+currentPos);
			if(data.caretPos < currentPos)
			{
				console.log('updating caret');
				currentPos = currentPos + ($scope.activeNote.note.length - oldLen);
			}
			// old dog knows the tricks

			$timeout(function() {
				setCaretPosition(txt_area, currentPos);
			}, 0);
		}
	});

	$scope.$on('socket:edit-course', function(event, data)
	{
		if($scope.activeCourseId == data.courseId)
		{
			$scope.notes = data.notes;
			angular.forEach(data.notes, function(value, key)
			{
				if($scope.activeNote._id == value._id)
				{
					var title = $scope.activeNote.title;
					var note = $scope.activeNote.note;
					$scope.activeNote = value;
					$scope.activeNote.title = title;
					$scope.activeNote.note = note;
				}
			});
		}
	});

	$scope.selectCourse = function($event, courseId){
		$scope.activeCourseId = courseId;
		$scope.activeNote = null;
		$scope.notes = [];
		$scope.joinSocket(courseId);
		$http.get(requestUrl + '/courses/' + $scope.activeCourseId + '/notes')
		.success(function(response){ $scope.notes = response; });
	};

	$scope.selectNote = function(note)
	{
		$scope.displayNote = true;
		$scope.showRevision = false;
		if($scope.activeNote !== note){
			$scope.activeNote = note;
		}
		else{
			$scope.activeNote = null;
		}

		lecSocket.emit('join-note', note._id);
	}

	$scope.saveNote = function(note)
	{
		note.courseId = $scope.activeCourseId;
		note.caretPos = getCaretPosition(document.querySelector('#note'));
		$scope.sentNotes.push(note.note);
		$scope.sendSocket(note);
	}

	$scope.createNew = function()
	{
		$http.post(requestUrl + '/courses/' + $scope.activeCourseId + '/notes/', {title : "", note: ""})
		.success(function(response) {
			$scope.notes.push(response);
			$scope.activeNote = response;
		});

	}
	$scope.sendSocket = function(note)
	{
		lecSocket.emit('edit', note);
	}
	$scope.joinSocket = function(courseId)
	{
		lecSocket.emit('join-course', courseId);
	}

	$scope.getSnapshots = function(noteId)
	{
		$scope.showRevision = true;
		$http.get(requestUrl + '/snapshots/' + noteId)
		.success(function(response){
			console.log(response);
			$scope.snapshots = response;
		});
	}

	$scope.restoreSnapshot = function(snapshot)
	{
		$scope.activeNote.title = snapshot.title;
		$scope.activeNote.note =  snapshot.note;
		$scope.showRevision = false;
		$scope.saveNote($scope.activeNote);
	}

	$scope.closeSnapshot = function()
	{
		$scope.showRevision = false;
	}
});
