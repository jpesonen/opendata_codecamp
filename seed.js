var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/lecnotes');

var Course = require('./model/course');

Course.remove({}, function(err) {
   console.log('Database cleared.') 
});

console.log("Creating new database with some seed");
new Course({
    name : "Games and Networking",
    code : "CT30A5002",
    notes: [],
}).save();
new Course({
    name : "Hajautetut järjestelmät",
    code : "CT30A3400",
    notes: [],
}).save();
new Course({
    name : "Parallel Computing",
    code : "CT30A7500",
    notes: [],
}).save();
new Course({
    name : "Wireless Service Engineering",
    code : "CT30A8301",
    notes: [],
}).save();
new Course({
    name : "Service Oriented Architecture",
    code : "CT30A8902",
    notes: [],
}).save();
new Course({
    name : "Software Engineering Methods",
    code : "CT60A5100",
    notes: [],
}).save();
new Course({
    name : "Critical Thinking and Argumentation in Software Engineering",
    code : "CT60A7000",
    notes: [],
}).save();
