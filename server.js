var mongoose = require('mongoose');
var express = require('express');
var bodyParser = require('body-parser');
var app = express();

mongoose.connect('mongodb://localhost/lecnotes');
var Course = require('./model/course.js');
var Snapshot = require('./model/snapshot.js')

app.use(bodyParser.json());

app.use("/", express.static('./app'));


/*
	POSTS (create)
*/

//create new note
app.post('/courses/:id/notes', function(req, res) {
	var course = Course.findById(req.params.id, function(err, course){
		if(err || !course)
			res.sendStatus(404);
		else
		{
			course.notes.push({
				title: req.body.title,
		    	note: req.body.note,
		    	created_at: new Date(),
				modified_at: new Date(),
			});
			course.save(function(err, course, count )
			{
				res.send(course.notes[course.notes.length-1]);
			});
		}
	});
});

/*
	GETS (fetch)
*/

//fetch note
app.get('/courses/:cid/notes/:nid', function(req, res) {
	Course.findById(req.params.cid, function(err, course)
	{
		if(err || !course)
			res.sendStatus(404);
		else
		{
			var note = course.notes.id(req.params.nid);
			if(!note)
				res.sendStatus(404);
			else
				res.send(note);
		}
	});
});
//fetch notes by course id
app.get('/courses/:cid/notes', function(req, res) {
	Course.findById(req.params.cid, function(err, course)
	{
		if(err || !course)
			res.sendStatus(404);
		else
			res.send(course.notes);
	});
});

app.get('/courses', function(req, res) {
	Course.find({}, function(err, courses){
		if(err || !courses)
			res.sendStatus(404);
		else
    		res.send(courses);
	});
});

app.get('/snapshots/:nid', function(req,res){
	Snapshot.find({noteId: req.params.nid}, function(err, snapshots){
		if(err || !snapshots)
			res.sendStatus(404);
		else
			res.send(snapshots);
	});
});

function updateNote(data, callback){
	data.modified_at = new Date();
	createSnapshot(data);
	Course.update({'notes._id': data._id}, 
	{
	    $set: { 
	        'notes.$': data,
	    }
	}, callback);
}

function createSnapshot(data){
	var latestSnapshot = null;
	var snapshot = Snapshot.find({noteId: data._id}, function(err, items) {
    	if(items.length > 0 ){
    		items.forEach(function(element){
    			if(element.note == data.note){ //already have one similar backup
    				return;
    			}
    		});
			latestSnapshot = items[items.length-1];
			var snapshotCreatedAt = new Date(latestSnapshot.created_at);
			var dataModifiedAt = new Date(data.modified_at);
			if(latestSnapshot.note !=  data.note && dataModifiedAt.getTime() - snapshotCreatedAt.getTime()  > 30000)
			{
				console.log('updated snapshot');
				newSnapshot(data);
			}
		}
		else{
			console.log('notes first snapshot');
			newSnapshot(data);
      }
    });
}

function newSnapshot(data){
	new Snapshot({
			noteId: data._id,
			title : data.title,
			note: data.note,
			created_at: new Date(),
	}).save();
}

/*
	PUTS (update)
*/

//update given note
app.put('/courses/:cid/notes/:nid', function(req, res) {

	var note_data = {
		title: req.body.title,
		note: req.body.note,
		_id: mongoose.Types.ObjectId(req.params.nid)
    };

    updateNote(note_data, function(err, query, count) {
    	res.send(note_data);
    });

});

var server = app.listen(8080, function () 
{
  var host = server.address().address;
  var port = server.address().port;

  console.log('Server started at http://%s:%s', host, port);

});

// Socket handling

var io = require('socket.io').listen(server);

io.on('connection', function(socket) {
	console.log('got socket: ' + socket.id);

	socket.on('join-course', function(data) {
		console.log('socket ' + socket.id + ' joined to course: ' + data );
		socket.join(data);
	});

	socket.on('join-note', function(data) {
		console.log('socket ' + socket.id + ' joined to note: ' + data );
		socket.join(data);
	});

	socket.on('edit', function(data) {
		var caretPos = data.caretPos;
		io.in(data._id).emit('edit', data);
		console.log("got caretPos: "+caretPos);
		var courseId = data.courseId;

		updateNote(data, function(err, query, count) {
			console.log('note ' + data._id  + ' updated by' + socket.id);
			Course.findById(courseId, function(err, course) {
				var response_data = {
					notes: course.notes,
					caretPos: caretPos,
					caretId: data._id,
					courseId: courseId
				}

				io.in(courseId).emit('edit-course', response_data);
			});
		});
	});
});
